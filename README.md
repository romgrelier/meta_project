# Projet de somme coloration - métaheuristique

## Installation
L'implémentation des algorithmes d'optimisation pour ce projet utilise le language julia v1.3.1, le choix de ce langage pour ce projet est du à sa vitesse d'execution jugée adaptée pour ce type de projet.

Julia utilise un interpréteur jit dans la version de référence est téléchargeable ici : https://julialang.org/downloads/ et la version utilisé "Generic Linux Binaries for x86 - 64bit".

Lors de l'execution d'un fichier, julia peut demander d'ajouter certaines bibliothèques, dans ce cas il suffit de suivre les instructions données dans le terminal en lançant l'interpréteur julia.

## Usage
Les différentes algorithmes ont chacun leurs fichier avec une instance prête à être lancés :
* gready_builder
* hill_climber
* tabu_search
* genetic

le fichier instance.jl contient des fonctions communes aux algorithmes et le chargeur de fichier, donnant une matrice d'adjacence.

Pour lancer un code julia il suffit de lancer
'''bash
  julia file
'''
