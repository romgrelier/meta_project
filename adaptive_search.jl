include("instance.jl")
include("tabu_list.jl")
include("greedy_builder.jl")

"""
Search for a better solution to satisfy the problem
"""
function find_better_neighbor_constraint(solution::Array{Int64, 1}, graph::Matrix{Bool}, color_count::Array{Int64, 1}, variable::Int64)
    neighbor::Array{Int64, 1} = copy(solution)
    solution_cost::Int64 = sum(compute_constraint_cost(solution, graph))
    neighbor_cost::Int64 = solution_cost
    improved::Bool = false

    # explore until finding a better color
    for c in 1:length(color_count)
        neighbor[variable] = c
        neighbor_cost = sum(compute_constraint_cost(neighbor, graph))

        if neighbor_cost < solution_cost
            # update the new solution
            improved = true
            solution = neighbor
            solution_cost = neighbor_cost

            # keep the color count
            color_count[c] += 1
            color_count[solution[variable]] -= 1

            break
        else
            # restore previous color
            neighbor[variable] = solution[variable]
        end
    end

    return improved, solution, solution_cost, objective_function(solution, color_count), solution[variable]
end

function find_better_neighbor_optimization(solution::Array{Int64, 1}, graph::Matrix{Bool}, color_count::Array{Int64, 1}, color::Int64)
    neighbor::Array{Int64, 1} = copy(solution)
    solution_cost::Int64 = objective_function(solution, color_count)
    neighbor_cost::Int64 = solution_cost
    improved::Bool = false
    variable::Int64 = 0

    for v in 1:length(solution)
        neighbor[v] = color

        # keep the color count
        color_count[color] += 1
        color_count[solution[v]] -= 1

        neighbor_cost = objective_function(neighbor, color_count)

        if neighbor_cost < solution_cost #&& sum(compute_constraint_cost(neighbor, graph)) == 0
            # update the new solution
            improved = true
            solution = neighbor
            solution_cost = neighbor_cost
            variable = v
            break
        else
            # restore previous color
            neighbor[v] = solution[v]
            # keep the color count
            color_count[color] -= 1
            color_count[solution[v]] += 1
        end
    end

    return improved, solution, sum(compute_constraint_cost(solution, graph)), solution_cost, variable
end

function solver(graph::Matrix{Bool}, tabu_size::Int64, tabu_iteration::Int64, MR::Int64, MI::Int64)
    restart::Int64 = 0

    # Best solution
    best_solution::Array{Int64, 1} = Array{Int64, 1}(undef, size(graph)[1]) # actual optimal solution
    best_solution_constraint_cost::Int64 = typemax(Int64) # constraint cost
    best_solution_objectif_cost::Int64 = typemax(Int64) # objective function cost

    # tabu list
    tabu_list_variable::Array{Int64, 1} = zeros(length(best_solution))
    tabu_list_color::Array{Int64, 1} = zeros(length(best_solution))

    # Restart loop
    while restart < MR
        restart += 1

        # random assignement
        actual_solution::Array{Int64, 1} = rand(1:size(graph)[1], size(graph)[1]) # solution to explore
        color_counter::Array{Int64, 1} = count_color(actual_solution)
        actual_constraint_cost = sum(compute_constraint_cost(actual_solution, graph)) # constraint cost
        actual_objectif_cost = objective_function(actual_solution, color_counter)

        # reset the tabu list
        fill!(tabu_list_variable, 0)
        fill!(tabu_list_color, 0)

        # Optimization/Satisfaction loop
        iteration::Int64 = 0
        while iteration < MI
            iteration += 1

            # println(color_counter)

            # compute the cost for each variable
            variable_cost::Array{Int64, 1} = compute_constraint_cost(actual_solution, graph)

            if sum(variable_cost) > 0 # satisfaction
                # println("Satisfaction")
                # select the worst variable not in the tabu list
                ordered_variables = sortperm(variable_cost, rev=true)

                worst_variable_index = 1
                worst_variable = ordered_variables[worst_variable_index]

                # search for a variable not in tabu_list with the order worst variables
                while worst_variable_index <= length(ordered_variables) && tabu_list_variable[worst_variable] > 0
                    worst_variable_index += 1
                    worst_variable = ordered_variables[worst_variable_index]
                end

                # evaluate the cost of possible move from X and give the best neighbor if exists
                improved, actual_solution, actual_constraint_cost, actual_objectif_cost, color = find_better_neighbor_constraint(actual_solution, graph, color_counter, worst_variable)

                # if no improvements exists
                if !improved
                    # println("Satisfaction - not improved")
                    # set the worst_variable as tabu for tabu_iteration
                    addVariable!(tabu_list_variable, worst_variable, tabu_iteration)

                    # if the tabu list is full, randomly reset variables
                    if tabuSizeExceed(tabu_list_variable, tabu_size)
                        # println("Satisfaction - reset")
                        resetVariable(tabu_list_variable, actual_solution, size(graph)[1], 0.5)
                    end
                end

            else # optimization
                # println("Optimization")
                color_counter = count_color(actual_solution)
                sorted_colors = sortperm(color_counter, rev=true)

                color_index = 1
                color = sorted_colors[color_index]

                # search for a variable not in tabu_list with the order worst variables
                while color_index <= length(sorted_colors) && tabu_list_color[color] > 0
                    color_index += 1
                    color = sorted_colors[color_index]
                end

                # evaluate the cost of possible move from X and give the best neighbor if exists
                improved, actual_solution, actual_constraint_cost, actual_objectif_cost, variable = find_better_neighbor_optimization(actual_solution, graph, color_counter, color)
                addVariable!(tabu_list_variable, variable, tabu_iteration)
                if tabuSizeExceed(tabu_list_variable, tabu_size)
                    resetVariable(tabu_list_variable, actual_solution, size(graph)[1], 0.3)
                end

                # if no improvements exists
                if !improved
                    # println("Optimization - not improved")
                    # set the worst_variable as tabu for tabu_iteration
                    addVariable!(tabu_list_color, color, tabu_iteration)

                    # if the tabu list is full, randomly reset variables
                    if tabuSizeExceed(tabu_list_color, tabu_size)
                        # println("Optimization - reset")
                        resetVariable(tabu_list_color, actual_solution, size(graph)[1], 0.5)
                    end
                end
            end

            # update tabu lists
            for i in 1:length(tabu_list_variable)
                if tabu_list_variable[i] > 0
                    tabu_list_variable[i] -= 1
                end
            end
            for i in 1:length(tabu_list_color)
                if tabu_list_color[i] > 0
                    tabu_list_color[i] -= 1
                end
            end

            # update global best global solution
            if actual_objectif_cost < best_solution_objectif_cost && actual_constraint_cost <= best_solution_constraint_cost
                best_solution = copy(actual_solution)
                best_solution_objectif_cost = actual_objectif_cost
                best_solution_constraint_cost = actual_constraint_cost
            end

            # println("objective cost : $(actual_objectif_cost)")
            # println("constraint cost : $(actual_constraint_cost)")

        end
    end

    return best_solution_objectif_cost, best_solution_constraint_cost, best_solution
end

function swap!(solution::Array{Int64}, v1::Int64, v2::Int64)
    solution[[v1, v2]] = solution[[v2, v1]]
end

tabu_size = 7
tabu_iteration = 7
MR = 10
MI = 1000

# anna : 276
# 125.5 : 1015
# 250.5 : 3246
# 500.5 : 10910
# 1000.5 : 37598
graph = load_instance("meta_project/graph_data/dsjc1000.9.col")

# Gready
@time solution, colors = greedy_builder(graph)

println("== Gready ==")
println("\tconstraint cost : $(sum(compute_constraint_cost(solution, graph)))")
println("\tobjective cost : $(objective_function(solution, length(solution)))")

# Adaptive Search
@time o_solution, c_solution, solution = solver(graph, tabu_size, tabu_iteration, MR, MI)

println("== Adaptive Search ==")
println("\tconstraint cost : $(c_solution)")
println("\tobjective cost : $(o_solution)")

print("\tsolution : $(check_solution(solution, graph))")


# println("k = $(k)")
# Threads.@threads for i = 1:4
#     @time cost_solution, solution = solver(graph, k, tabu_size, tabu_iteration, MR, MI)
# end
