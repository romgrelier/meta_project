include("instance.jl")

function utility(solution::Array{Int64, 1}, graph::Matrix{Bool}, color::Int64)::Float64
    solution[end] = color

    if check_solution(solution, graph)
        return 1 / objective_function(solution, size(graph)[1])
    else
        return 0
    end
end

"""
Build a randomly gready solution
"""
function greedyRandomizedConstruction(graph::Matrix{Bool}, α::Float64)::Array{Int64, 1}
    solution::Array{Int64, 1} = Array{Int64, 1}(undef, size(graph)[1])
    colors::Array{Int64, 1} = collect(1:length(solution))

    for v in 1:length(solution)
        utilities::Array{Float64, 1} = [utility(solution[1:v], graph, c) for c in 1:length(colors)]
        limit = minimum(utilities) + α * (maximum(utilities) - minimum(utilities))

        rcl = [i for i in 1:length(colors) if utilities[i] >= limit]
        solution[v] = colors[rand(rcl)]
    end

    return solution
end


"""
    explore the neighborhood of a a solution
"""
function localSearchImprovement(solution::Array{Int64, 1}, graph::Matrix{Bool})::Array{Int64, 1}

end

"""
Grasp algorithm
α ∈ [0, 1] is the tradeoff between gready and random (0 : random, 1 gready)
"""
function grasp(graph::Matrix{Bool}, max_iter::Int64, α::Float64)
    best_solution = Array{Int64, 1}(undef, size(graph)[1])

    for i in 1:max_iter
        solution = greedyRandomizedConstruction(graph, α)
        solution = localSearchImprovement(solution, graph)

    end
end

graph = load_instance("meta_project/graph_data/anna.col")

solution = greedyRandomizedConstruction(graph, 0.6)
println(check_solution(solution, graph))
println(objective_function(solution, length(solution)))

@time solution = grasp(graph, 100, 0.2)
