function addVariable!(tabu_list, variable, iteration)
    tabu_list[variable] = iteration
end

function inTabuList(tabu_list, variable)
    tabu_list[variable] > 0
end

function tabuSizeExceed(tabu_list, max_size)
    size = 0
    for v in 1:length(tabu_list)
        if inTabuList(tabu_list, v)
            size += 1
        end
    end

    size >= max_size
end

function resetVariable(tabu_list, solution, k, p)
    # count of variable to reset
    count::Int64 = ceil(length(tabu_list) * p)

    # tabu variable
    tabu_variable = [v for v in 1:length(tabu_list) if inTabuList(tabu_list, v)]

    # println(tabu_variable)

    # choosen variabe to reset
    to_reset = rand(tabu_variable, count)

    # reset choosen variables
    for v in to_reset
        # randomly reset v
        solution[v] = rand(1:k)
        # remove v from the tabu list
        tabu_list[v] = 0
    end
end
