include("instance.jl")

mutable struct Individual
    solution::Array{Int64, 1}
    fitness::Int64
end

function generate_population(pop_size::Int64, genotype_size::Int64, color_range::Int64)::Array{Individual, 1}
    return [Individual(rand(1:color_range, size(graph)[1]), typemax(Int64)) for _ in 1:pop_size]
end

function evaluate_population!(population::Array{Individual, 1}, graph::Matrix{Bool})
    for p in population
        p.fitness = objective_function_penalized(p.solution, graph)
    end
end

function selection(population::Array{Individual, 1})::Tuple{Individual, Individual}
    randomly_selected::Array{Individual, 1} = rand(population, 10)

    sort!(randomly_selected, by = x -> x.fitness)

    return (randomly_selected[1], randomly_selected[2])
end

function crossover(parents::Tuple{Individual, Individual}, graph::Matrix{Bool})::Tuple{Individual, Individual}
    genotype_size::Int64 = length(parents[1].solution)

    offspring = [
        Individual(Array{Int64, 1}(undef, genotype_size), 0) for _ in 1:2
    ]

    cut = Int64(floor(genotype_size/2))
    println(cut)
    cut = rand(1:cut)
    println(cut)

    offspring[1].solution[1:cut] = parents[1].solution[cut+1:end]
    offspring[1].solution[cut+1:end] = parents[2].solution[1:cut]

    offspring[2].solution[1:cut] = parents[2].solution[cut+1:end]
    offspring[2].solution[cut+1:end] = parents[1].solution[1:cut]

    offspring[1].fitness = objective_function_penalized(offspring[1].solution, graph)
    offspring[2].fitness = objective_function_penalized(offspring[2].solution, graph)

    return (offspring[1], offspring[2])
end

function mutation!(offspring::Tuple{Individual, Individual}, color_range::Int64)
    for o in offspring
        if rand() > 0.3
            o.solution[rand(1:length(o.solution))] = rand(1:color_range)
            o.fitness = objective_function_penalized(o.solution, graph)
        end
    end
end

function insertion!(population::Array{Individual, 1}, offspring::Tuple{Individual, Individual})
    sort!(population, by = x -> x.fitness)

    for o in offspring
        if o.fitness < population[end].fitness
            population[end] = o
            sort!(population, by = x -> x.fitness)
        end
    end
end

function genetic(graph::Matrix{Bool}, max_iter::Int64, pop_size::Int64, color_range::Int64)
    history::Array{Int64, 1} = Array{Int64, 1}(undef, max_iter)

    population::Array{Individual, 1} = generate_population(pop_size, size(graph)[1], color_range)

    # for p in population
    #     repair_solution!(p.solution, graph)
    #     p.fitness = objective_function_penalized(p.solution, graph)
    # end

    evaluate_population!(population, graph)

    for iter in ProgressBar(1:max_iter)
        parents::Tuple{Individual, Individual} = selection(population)

        offspring::Tuple{Individual, Individual} = crossover(parents, graph)

        mutation!(offspring, color_range)

        insertion!(population, offspring)

        history[iter] = population[1].fitness
    end

    return population[1].solution, history
end

graph = load_instance("meta_project/graph_data/huck.col")

max_iter = 100000
pop_size = 200
color_range = size(graph)[1]

solution, history = genetic(graph, max_iter, pop_size, color_range)
println("== RUN ==")
println("constraint cost : $(sum(compute_constraint_cost(solution, graph)))")
println("objective cost : $(objective_function(solution))")

plot(1:max_iter, history)

repair_solution!(solution, graph)
println("after repair")
println("constraint cost : $(sum(compute_constraint_cost(solution, graph)))")
println("objective cost : $(objective_function(solution))")

p_pop_size = [10, 50, 100, 150, 200]

performance = Matrix{Int64}(undef, max_iter, length(p_pop_size))

for i in 1:length(p_pop_size)
    solution, history = genetic(graph, max_iter, p_pop_size[i], color_range)
    performance[:, i] = history[:]
end

plot(performance, label =  ["10" "50" "100" "150" "200"])
savefig("genetic_pop.png")
