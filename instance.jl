using ProgressBars
using Plots

"""
Loads a DIMACS file format to an adjacency matrix
"""
function load_instance(filename::String)::Matrix{Bool}
    file = open(filename)

    lines = readlines(file)

    close(file)

    matrix::Matrix{Bool} = zeros(1, 1)

    for line in lines
        if line[1] == 'p'
            splitted = split(line)
            size::Int64 = parse(Int, splitted[3])
            matrix = zeros(size, size)
        elseif line[1] == 'e'
            splitted = split(line)
            x::Int64 = parse(Int, splitted[2])
            y::Int64 = parse(Int, splitted[3])
            matrix[x, y] = true
            matrix[y, x] = true
        end
    end

    return matrix
end

"""
Compute the cost of each node connected with the same color
"""
function compute_constraint_cost(solution::Array{Int64, 1}, graph::Matrix{Bool})::Array{Int64, 1}
    return [variable_cost(solution, graph, v) for v in 1:length(solution)]
end

"""
Compute the cost for one variable
"""
function variable_cost(solution::Array{Int64, 1}, graph::Matrix{Bool}, variable::Int64)::Int64
    return sum([1 for v in 1:length(solution) if solution[variable] == solution[v] && graph[variable, v]])
end

"""
Check if a given solution respects all constraints
"""
function check_solution(solution::Array{Int64, 1}, graph::Matrix{Bool})::Bool
    sum(compute_constraint_cost(solution, graph)) == 0
end

"""
Count how many each color is in the solution
"""
function count_color(solution::Array{Int64, 1}, color_count::Int64)::Array{Int64, 1}
    counter::Array{Int64, 1} = zeros(color_count)

    for i in solution
        counter[i] += 1
    end

    return counter
end

function count_color(solution::Array{Int64, 1})::Array{Int64, 1}
    counter::Array{Int64, 1} = zeros(length(solution))

    for i in solution
        counter[i] += 1
    end

    return counter
end

"""
Cost function for optimization
"""
function objective_function(solution::Array{Int64, 1}, color_count::Array{Int64, 1})::Int64
    color_count = count_color(solution)

    counter = sort(color_count, rev=true)

    return sum([i * counter[i] for i in 1:length(counter)])
end

function objective_function(solution::Array{Int64, 1}, color_count::Int64)::Int64
    color_count = count_color(solution, color_count)

    counter = sort(color_count, rev=true)

    return sum([i * counter[i] for i in 1:length(counter)])
end

function objective_function(solution::Array{Int64, 1})::Int64
    color_count = count_color(solution, length(solution))

    counter = sort(color_count, rev=true)

    return sum([i * counter[i] for i in 1:length(counter)])
end

function objective_function_penalized(solution::Array{Int64, 1}, graph::Matrix{Bool})::Int64
    color_count = count_color(solution, length(solution))

    counter = sort(color_count, rev=true)

    return sum([i * counter[i] for i in 1:length(counter)]) + sum(compute_constraint_cost(solution, graph)) ^ 10
end

function repair_solution!(solution::Array{Int64, 1}, graph::Matrix{Bool})
    variables_cost = compute_constraint_cost(solution, graph)
    color_count = count_color(solution)
    color_order = sortperm(color_count, rev=true)

    for v in 1:length(variables_cost)
        if variables_cost[v] > 0
            # search for another color
            for c in color_order
                former_color = solution[v]
                solution[v] = c

                if variable_cost(solution, graph, v) == 0
                    color_count[c] += 1
                    color_count[former_color] -= 1
                    color_order = sortperm(color_count, rev=true)
                    break
                else
                    solution[v] = former_color
                end
            end
        end
    end
end
