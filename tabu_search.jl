include("instance.jl")

"""
Search for the best neighbor, may be worse than the actual solution
"""
function best_neighbor(solution::Array{Int64, 1}, cost::Int64, graph::Matrix{Bool}, tabu_list::Array{Tuple{Int64, Int64}, 1}, color_range::Int64)::Tuple{Int64, Int64, Int64}
    best_move::Tuple{Int64, Int64} = (0, 0)
    best_cost::Int64 = typemax(Int64)

    # search for the best move
    for v in 1:length(solution) # for each variable
        former_color = solution[v]
        for c in 1:color_range # for each color
            if !((v, c) in tabu_list)
                # evaluate neighbor
                solution[v] = c
                new_cost = objective_function_penalized(solution, graph)

                if new_cost < best_cost # update the best move found
                    best_move = (v, c)
                    best_cost = new_cost
                end
            end
        end
        solution[v] = former_color
    end

    v, c = best_move
    return v, c, best_cost
end

function tabu_search(graph::Matrix{Bool}, max_iter::Int64, tabu_size::Int64, color_range::Int64)
    best_solution::Array{Int64, 1} = Array{Int64, 1}(undef, size(graph)[1])
    best_cost::Int64 = typemax(Int64)

    solution::Array{Int64, 1} = rand(1:color_range, size(graph)[1])
    cost::Int64 = objective_function_penalized(solution, graph)
    tabu_list::Array{Tuple{Int64, Int64}, 1} = []

    history::Array{Int64, 1} = Array{Int64, 1}(undef, max_iter)

    for iter in ProgressBar(1:max_iter)
        # search for the best move
        v, c, neighbor_cost = best_neighbor(solution, cost, graph, tabu_list, color_range)

        # update the tabu list
        push!(tabu_list, (v, c))
        if length(tabu_list) > tabu_size
            popfirst!(tabu_list)
        end

        # update the solution with the best move
        solution[v] = c
        cost = neighbor_cost

        # update if needed the best solution
        if cost < best_cost
            best_solution[:] = solution[:]
            best_cost = cost
        end
        history[iter] = cost
    end

    return solution, history
end

graph = load_instance("meta_project/graph_data/david.col")

max_iter = 200
tabu_size = 7
color_range = size(graph)[1]
@time solution, history = tabu_search(graph, max_iter, tabu_size, color_range)
println("== RUN ==")
println("constraint cost : $(sum(compute_constraint_cost(solution, graph)))")
println("objective cost : $(objective_function(solution))")

plot(1:max_iter, history)

repair_solution!(solution, graph)
println("after repair")
println("constraint cost : $(sum(compute_constraint_cost(solution, graph)))")
println("objective cost : $(objective_function(solution))")
