include("instance.jl")

function check_neighborhood(solution::Array{Int64, 1}, node::Int64, color::Int64, graph::Matrix{Bool})::Bool
    neighborhood::Array{Int64, 1} = findall(x -> x == true, graph[node, :])

    for n in neighborhood
        if solution[node] == solution[n]
            return false
        end
    end

    return true
end

function get_graph_degree(graph::Matrix{Bool})::Array{Int64, 1}
    degree::Array{Int64, 1} = zeros(size(graph)[1])

    for i in 1:length(degree)
        degree[i] = sum(graph[i, :])
    end

    return degree
end

function greedy_builder(graph::Matrix{Bool})::Tuple{Array{Int64, 1}, Int64}
    solution::Array{Int64, 1} = zeros(size(graph)[1])
    colors::Int64 = 1

    degrees = get_graph_degree(graph)
    order = sortperm(degrees, rev=true)
    for n in order
    # for n in 1:length(solution)
        assigned::Bool = false

        for k in 1:colors
            solution[n] = k
            if check_neighborhood(solution, n, k, graph)
                assigned = true
                solution[n] = k
                break
            end
        end

        if !assigned
            colors += 1
            solution[n] = colors
        end
    end

    return solution, colors
end

graph = load_instance("meta_project/graph_data/david.col")

solution, _ = greedy_builder(graph)
println("== RUN ==")
println("constraint cost : $(sum(compute_constraint_cost(solution, graph)))")
println("objective cost : $(objective_function(solution))")
